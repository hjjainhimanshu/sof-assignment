/**
 * GET /*
 * Home page.
 */

'use strict';
const models = require('../models/');

/**
 *
 * @type {{index: Function}}
 */
module.exports = {
  register: function (req, res, next) {
  	models.User.create(req.body).then(user => {
      return res.render('student', { title: 'School of Future', user: user, message: 'Hello '+user.firstName});
  	}).catch(err => {
      return res.render('login', {  message: 'Registeration Failed'});
    });
  },
  update: function (req, res, next) {
    models.User.update(req.body, {where: {id:req.params.id}}).then(user => {
      req.session.passport.user.ikigaiObj = req.body.ikigaiObj;
      req.session.save(function(err) {console.log(err);});
      return res.json({'message': 'Updated Post', 'result': user, 'error': false});
    }).catch(err =>  {
        console.log(err);
        return res.json({'message': 'Server Error', 'result': [], 'error': true}).status(500);
    });
  },
  get: function(req, res, next) {
    models.User.findAll({where:{userType: 1}}).then(users => {
      console.log(users);
      return res.render('users', {  users: users});
    }).catch(err =>  {
        console.log(err);
      return res.json({'message': 'Server Error', 'result': [], 'error': true}).status(500);
    });
  }
};
