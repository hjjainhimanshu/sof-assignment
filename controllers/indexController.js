/**
 * GET /*
 * Home page.
 */

'use strict';
const User = require('../models/user');

/**
 *
 * @type {{index: Function}}
 */
module.exports = {
  index: function (req, res, next) {
  	if(req.user.userType == 2) {
  		return res.render('index', {title: 'School of Future', user: req.user, message: 'Hello '+req.user.firstName});
  	} else {
  		return res.render('student', {title: 'School of Future', user: req.user, message: 'Hello '+req.user.firstName});
  	}
    
  },
  login: function (req, res, next) {
    return res.render('login');
  },
};
