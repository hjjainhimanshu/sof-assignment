'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */
      return queryInterface.createTable('user', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        firstName: {
          type: Sequelize.STRING(100)
        },
        lastName: {
          type: Sequelize.STRING(100)
        },
        email: {
          type: Sequelize.STRING(250),
          unique: true
        },
        organisation: {
          type: Sequelize.STRING,
          allowNull: true
        },
        password: {
          type: Sequelize.STRING
        },
        userType: {
          type: Sequelize.INTEGER,
          defaultValue: 0
        },
        ikigaiObj: {
          type: Sequelize.JSON,
          allowNull: true,
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      });
    
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */
      return queryInterface.dropTable('user');
    
  }
};
