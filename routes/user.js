var express = require('express');
var router = express.Router();
var passport = require('../config/passport');
var userController = require('../controllers/userController'); 

/* GET users listing. */
router.get('/', userController.get);

router.post('/login',
  passport.authenticate('local', { successRedirect: '/',
                                   failureRedirect: '/login',
                                   failureFlash: true })
);
router.post('/register', userController.register);
router.put('/:id', userController.update);

module.exports = router;
