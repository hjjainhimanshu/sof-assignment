'use strict';

const Sequelize = require('sequelize');
const bcrypt = require('bcrypt');

function encryptPasswordIfChanged(user, options) {
  if (user.changed('password')) {
    const salt = bcrypt.genSaltSync();
    user.password = bcrypt.hashSync(user.password, salt);
  } 
}

module.exports = function(sequelize, DataTypes) {

  const User = sequelize.define('User', {
  	firstName: {
      type: Sequelize.STRING(100)
    },
    lastName: {
      type: Sequelize.STRING(100)
    },
    email: {
    	type: Sequelize.STRING(250),
      allowNull: false,
      unique: true,
    	validate: { 
    		isEmail: {
          args: true,
          msg:  "Email is not valid"
        }
    	}
    },
    password: {
    	type: Sequelize.STRING,
      allowNull: false,
      validate: {
        min: {
          args: 6,
          msg: "Password must be more than 6 characters"
        }
      }
    },
    organisation: {
      type: Sequelize.STRING,
      allowNull: true,
      validate: {
        min: {
          args: 3,
          msg: "organisation must be more than 3 characters"
        }
      }
    },
    userType: {
    	type: Sequelize.INTEGER,
    	validate: {
    		max: 2,  
        min: 0
    	},
    	defaultValue: 0, // anyone/everyone can be volunteer
    }, // 0 - volunteer, 1 - student, 2 - admin
    ikigaiObj: {
      type: Sequelize.JSON,
      allowNull: true,
    }
  }, {
    timestamps: true,
    tableName: "user",
    classMethods: {
      associate: function (models) {
        models.User.hasOne(models.Email, {foreignKey: "userId", as: 'Email'});
      }
    },
    hooks: {
      beforeCreate: function(user){       
        encryptPasswordIfChanged(user);
      },
      beforeUpdate: function(user){
        encryptPasswordIfChanged(user);
      }
    }
  });

  User.prototype.comparePassword = function(password) {
    return bcrypt.compareSync(password, this.password);
  };

  User.prototype.toJSON = function() {
    var values = Object.assign({}, this.get());
      delete values.password;
      return values;
  };

  return User;
}