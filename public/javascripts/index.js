
function toJSON( form ) {
  var obj = {};
  var elements = form.querySelectorAll( "input, select, textarea" );
  for( var i = 0; i < elements.length; ++i ) {
    var element = elements[i];
    var name = element.name;
    var value = element.value;
    if( name ) {
      obj[ name ] = value;
    }
  }
  return obj;
  //return JSON.stringify( obj );
}

 $.ajax({
    url: '/user',
    type: 'GET',
    success: function(result) {
      console.log(result)
      $("#users-container").append(result)
      // Do something with the result
    }
  });
