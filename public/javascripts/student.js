/* The dragging code for '.draggable' from the demo above
 * applies to this demo as well so it doesn't have to be repeated. */
// enable draggables to be dropped into this
interact('.dropzone').dropzone({
  // only accept elements matching this CSS selector
  accept: '.yes-drop',
  // Require a 75% element overlap for a drop to be possible
  overlap: 0.75,

  // listen for drop related events:

  ondropactivate: function (event) {
    // add active dropzone feedback
    event.target.classList.add('drop-active')
  },
  ondragenter: function (event) {
    var draggableElement = event.relatedTarget;
    var dropzoneElement = event.target;

    // feedback the possibility of a drop
    dropzoneElement.classList.add('drop-target')
    draggableElement.classList.add('can-drop')
    console.log(event.target);
    // draggableElement.textContent = 'Dragged in'
  },
  ondragleave: function (event) {
    // remove the drop feedback style
    event.target.classList.remove('drop-target')
    event.relatedTarget.classList.remove('can-drop')
    event.relatedTarget.classList.remove('skills-you-love');
    event.relatedTarget.classList.remove('skills-you-are-good-at');
    event.relatedTarget.classList.remove('skills-you-can-be-paid-for');
    event.relatedTarget.classList.remove('skills-the-world-needs');
    // event.relatedTarget.textContent = 'Dragged out'
  },
  ondrop: function (event) {
    // event.relatedTarget.textContent = 'Dropped';
    var targetCircle = event.target.getAttribute('data-class');
    event.relatedTarget.classList.remove('skills-you-love');
    event.relatedTarget.classList.remove('skills-you-are-good-at');
    event.relatedTarget.classList.remove('skills-you-can-be-paid-for');
    event.relatedTarget.classList.remove('skills-the-world-needs');
    console.log(event.relatedTarget.classList);
    event.relatedTarget.classList.add(targetCircle);

    ikigaiObj['skills-you-love'] = findObject('skills-you-love');
    ikigaiObj['skills-you-are-good-at'] = findObject('skills-you-are-good-at');
    ikigaiObj['skills-you-can-be-paid-for'] = findObject('skills-you-can-be-paid-for');
    ikigaiObj['skills-the-world-needs'] = findObject('skills-the-world-needs');

    ikigaiObj['passion'] = intersectionObject(ikigaiObj['skills-you-love'], ikigaiObj['skills-you-are-good-at']);
    ikigaiObj['profession'] = intersectionObject(ikigaiObj['skills-you-are-good-at'], ikigaiObj['skills-you-can-be-paid-for']);
    ikigaiObj['vocation'] = intersectionObject(ikigaiObj['skills-you-can-be-paid-for'], ikigaiObj['skills-the-world-needs"']);
    ikigaiObj['mission'] = intersectionObject(ikigaiObj['skills-the-world-needs'], ikigaiObj['skills-you-love']);
    ikigaiObj['purpose'] = intersectionObject(ikigaiObj['profession'], ikigaiObj['mission']);
    renderObject();
  },
  ondropdeactivate: function (event) {
    // remove active dropzone feedback
    event.target.classList.remove('drop-active')
    event.target.classList.remove('drop-target')
  }
});

interact('.drag-drop')
  .draggable({
    inertia: true,
    modifiers: [
      interact.modifiers.restrict({
        restriction: 'self',
        endOnly: true,
        elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
      })
    ],
    autoScroll: true,
    // dragMoveListener from the dragging demo above
    onmove: dragMoveListener
  });

function dragMoveListener (event) {
  var target = event.target,
      // keep the dragged position in the data-x/data-y attributes
      x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
      y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

  // translate the element
  target.style.webkitTransform =
  target.style.transform = 'translate(' + x + 'px, ' + y + 'px)';

  // update the posiion attributes
  target.setAttribute('data-x', x);
  target.setAttribute('data-y', y);
}

// this is used later in the resizing and gesture demos
window.dragMoveListener = dragMoveListener;

var appendHtml = "";
var pickup = document.getElementById("pickup");
document.getElementById("skills-target").addEventListener("keyup", function(event) {
  event.preventDefault();
  if (event.keyCode === 13) {
    console.log("Handler for .keypress() called.");
    appendHtml += "<div class='col-sm-3'>";
    appendHtml += " <div class='drag-drop yes-drop'>"+ event.target.value+" </div>";
    appendHtml += " <div class='drag-drop yes-drop'>"+ event.target.value+" </div>";
    appendHtml += " <div class='drag-drop yes-drop'>"+ event.target.value+" </div>";
    appendHtml += " <div class='drag-drop yes-drop'>"+ event.target.value+" </div>";
    appendHtml += "</div>";
    pickup.innerHTML = appendHtml;
    $(this).val('');
  }
});


// function intersectionKeys(o1, o2) {
//   return Object.keys(o1).concat(Object.keys(o2)).sort().reduce(function (r, a, i, aa) {
//     if (i && aa[i - 1] === a) {
//       r.push(a);
//     }
//     return r;
//   }, []);
// }


function intersectionObject(o1, o2) {
  if(typeof o1 !== "undefined" && typeof o2 !== "undefined"){
    return Object.keys(o1).concat(Object.keys(o2)).sort().reduce(function (r, a, i, aa) {
      if (i && aa[i - 1] === a) {
        r[a] = o1[a];
      }
      return r;
    }, {});
  } else{
    return {};
  }
   
}


function findObject(cls){
  var divs = [].slice.call(document.getElementsByClassName(cls));
  var obj = divs.reduce(function(acc, cur, i) {
    acc[cur.innerHTML.trim()] = cur.innerHTML.trim();
    return acc;
  }, {});
  console.log(obj);

  return obj;
}

$('.js-showSecond').click(function(event) {
  event.preventDefault();
  $('.js-input-fields').hide();
  $('img').hide();
  $('.second-step').addClass('open');
});

function renderObject() {
  var appendHtml = "<table class='table'>";
  for(var key in ikigaiObj){
    appendHtml += "<tr><td>" + key.charAt(0).toUpperCase() + key.slice(1).split('-').join(' ') + ":</td>";
    // appendHtml += "<td><table class='table'><tr>"
    // for(var nextKey in ikigaiObj[key]) {
    //   appendHtml += "<td>" + nextKey + "</td>";
    //   // appendHtml += "<td>" + ikigaiObj[key][nextKey] + "</td>";
    //   // appendHtml += "";
    // }
    // appendHtml += "</tr></table></td></tr>";
    appendHtml += "<td>";
    for(var nextKey in ikigaiObj[key]) {
      appendHtml += "" + nextKey + ", ";
      // appendHtml += "<td>" + ikigaiObj[key][nextKey] + "</td>";
      // appendHtml += "";
    }
    appendHtml += "</td>";
    
  }
    appendHtml += "</table>";
    $('.js-results').html(appendHtml);
}

$('#saveButton').click(function(){
  var userId = document.getElementById("userId").value;
  $.ajax({
    url: '/user/' + userId,
    type: 'PUT',
    data: JSON.stringify({'ikigaiObj': ikigaiObj}),
    datatype: 'json',
    contentType: "application/json; charset=utf-8",
    success: function(result) {
      // Do something with the result
    }
  });
});

$('.js-tryagain').click(function(){
  $('.enter-skills').removeClass('hide');
  $('.js-tryagain').addClass('hide');
  ikigaiObj = {};
  renderObject();
});

renderObject();
